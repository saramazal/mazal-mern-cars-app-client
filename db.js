require('dotenv').config({ path: './.env' })
const mongoose = require('mongoose')



mongoose.connect(process.env.CONNECTION_STRING)
.then(() => console.log('MongoDB Connect! Happy Hacking!'))


module.exports = mongoose